# -*- coding: utf-8 -*-
import re

from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import User

email_re = re.compile(

    r"(^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*"  # dot-atom

    # quoted-string, see also http://tools.ietf.org/html/rfc2822#section-3.2.5

    r'|^"([\001-\010\013\014\016-\037!#-\[\]-\177]|\\[\001-\011\013\014\016-\177])*"'



    r')@((?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)$)'  # domain

    r'|\[(25[0-5]|2[0-4]\d|[0-1]?\d?\d)(\.(25[0-5]|2[0-4]\d|[0-1]?\d?\d)){3}\]$', re.IGNORECASE)


class UsersAuthenticationBackend(ModelBackend):
    """
    Позволяет входить пользователям по ``email`` или ``логину``
    
    """

    def authenticate(self, identification, password):

        """
        Авторизует пользователей по email/username и паролю.
        
        :param identification:
            Логин или email пользователя. Тип: строка.
        
        :password:
            Пароль пользователя. Тип: строка. Необязательный параметр.
        
        :return: Авторизованный пользователь (:class:`User`).
        
        """
        if email_re.search(identification):
            try:
                user = User.objects.get(email__iexact=identification)
            except User.DoesNotExist:
                return None
        else:
            try:
                user = User.objects.get(username__iexact=identification)
            except User.DoesNotExist:
                return None

        if user.check_password(password):
            return user
        else:
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
