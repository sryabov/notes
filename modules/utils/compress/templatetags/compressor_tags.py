from compressor.templatetags.compress import CompressorNode as BaseCompressorNode
from django import template

register = template.Library()


OUTPUT_FILE = 'file'
OUTPUT_INLINE = 'inline'
OUTPUT_URL = 'url'
OUTPUT_MODES = (OUTPUT_FILE, OUTPUT_INLINE, OUTPUT_URL)


class CompressorNode(BaseCompressorNode):
    def render(self, context, forced=False):
        rendered_output = super(CompressorNode, self).render(context, forced)
        if self.name:
            compressed = context.get('static', {})
            compressed.update({self.name: rendered_output})
            context['static'] = compressed
            return ''
        return rendered_output


@register.tag
def compress(parser, token):
    nodelist = parser.parse(('endcompress',))
    parser.delete_first_token()

    args = token.split_contents()

    if not len(args) in (2, 3, 4):
        raise template.TemplateSyntaxError(
            "%r tag requires either one, two or three arguments." % args[0])

    kind = args[1]

    if len(args) >= 3:
        mode = args[2]
        if mode not in OUTPUT_MODES:
            raise template.TemplateSyntaxError(
                "%r's second argument must be '%s' or '%s'." %
                (args[0], OUTPUT_FILE, OUTPUT_INLINE))
    else:
        mode = OUTPUT_FILE
    if len(args) == 4:
        name = args[3]
    else:
        name = None
    return CompressorNode(nodelist, kind, mode, name)

