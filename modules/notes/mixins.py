from django.utils.functional import cached_property

from ..notes.forms import ReportNoterForm


class ReportNoterMixin():
    form = None
    filters = {}

    def dispatch(self, request, *args, **kwargs):
        self.form = ReportNoterForm(request, self.request.GET)
        if self.form.is_valid():
            self.filters = self.form.cleaned_data
        return super(ReportNoterMixin, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ReportNoterMixin, self).get_context_data()
        context.update({
            'form': self.form,
        })
        return context

    @cached_property
    def date_from(self):
        return self.filters.get('date_from')

    @cached_property
    def date_to(self):
        return self.filters.get('date_to')

    @cached_property
    def title(self):
        if self.filters.get('title') == '0':
            return None
        return self.filters.get('title')

    @cached_property
    def category(self):
        if self.filters.get('category') == '0':
            return None
        return self.filters.get('category')

    @cached_property
    def favorite(self):
        if self.filters.get('favorite') == '0':
            return None
        elif self.filters.get('favorite') == '1':
            return True
        else:
            return False

    def filtred_queryset(self, queryset):
        if self.date_from:
            queryset = queryset.filter(created_date__gte=self.date_from)
        if self.date_to:
            queryset = queryset.filter(created_date__lte=self.date_to)
        if self.title:
            queryset = queryset.filter(title=self.title)
        if self.category:
            queryset = queryset.filter(category=self.category)
        if self.favorite:
            queryset = queryset.filter(favorite=self.favorite)
        return queryset
