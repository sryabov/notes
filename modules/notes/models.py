from uuid import uuid4

from ckeditor.fields import RichTextField
from django.conf import settings
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import models
from django.utils import timezone


class Category(models.Model):
    name = models.CharField(verbose_name='Название', max_length=100)

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name


class Note(models.Model):
    user = models.ForeignKey(User, verbose_name='Пользователь')
    title = models.CharField(verbose_name='Заголовок', max_length=100)
    content = RichTextField(verbose_name='Содержание')
    created_date = models.DateTimeField(verbose_name='Дата создания', default=timezone.now)
    category = models.ForeignKey(Category, verbose_name='Категория')
    favorite = models.BooleanField(verbose_name='Избранная', default=False)
    uuid = models.UUIDField('UUID', default=uuid4, unique=True)
    is_published = models.BooleanField('Опубликовано', default=False)

    class Meta:
        verbose_name = 'Заметка'
        verbose_name_plural = 'Заметки'
        ordering = ['-created_date']

    def __str__(self):
        return self.title

    @property
    def get_published_url(self):
        if self.is_published:
            path = reverse('notes_view', kwargs={'uuid':self.uuid})
            print('//////', path)
            print('-------------------', '{0}{1}'.format(settings.DOMAIN, path))
            return '{0}{1}'.format(settings.DOMAIN, path)
        else:
            return None
