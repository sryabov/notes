from .base import *

DEBUG = True

INSTALLED_APPS += (
    'debug_toolbar',
)

MIDDLEWARE += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

INTERNAL_IPS = ('127.0.0.1')

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

COMPRESS_OFFLINE = False
COMPRESS_ENABLED = False
