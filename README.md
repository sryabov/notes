# Приложение Заметки

## Установка в dev-среде
### Получение исходников и установка зависимостей
Создаем виртуальное окружение:

`virtualenv -p puthon3 /path/to/env`

Активруем его:

`source /path/to/env/Scripts/activate`

Переходим в каталог проекта:

`cd /path/to/notes`

Инсталируем зависмости:

`pip install -r requirements/dev.txt`

Переходим к настройкам

### local.py
Создаем файл `configs/settings/local.py`

В нем необходимо указать следующие настройки:

```python
from .dev import *

SECRET_KEY = 'случайно_сгенерированный_ключ'

ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, '../../db.sqlite3'),
    }
}

REDIS_PASSWORD = ''
REDIS_HOST = 'localhost'
REDIS_PORT = 6379
REDIS_DB = 1
REDIS_KEY_PREFIX = 'note'

SESSION_ENGINE = 'redis_sessions.session'
SESSION_REDIS_PREFIX = 'note_session'
SESSION_SAVE_EVERY_REQUEST = True
SESSION_REDIS_HOST = REDIS_HOST
SESSION_REDIS_PASSWORD = REDIS_PASSWORD
SESSION_REDIS_DB = REDIS_DB
SESSION_COOKIE_AGE = 86400


DOMAIN='http://127.0.0.1:8000'
```



